let defaultConfig = {
    broker: {
        host: 'localhost',
        port: 9001
    },
    clientId: 'admin',
    nodes: {
        control: 'control',
        launchpad: 'launchpad',
        karaoke: 'karaoke',
        beer: 'beer'
    },
    confirmation: true
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// Takes a number of seconds and returns a string "mm:ss"
// e.g.: 126 => "02:06"
function stopwatchString(seconds) {
    let str = "";
    if (seconds < 0) {
        str += seconds;
        str += "s";
    }
    else {
        let minutes = Math.floor(seconds / 60);
        let remSeconds = seconds % 60;
        if (minutes < 10) {
            str += "0";
        }
        str += minutes;
        str += ":";
        if (remSeconds < 10) {
            str += "0";
        }
        str += remSeconds;
    }
    return str;
}

class RoomAdmin {
    constructor(brokerHostname, brokerPort, clientId, nodeControl, nodeLaunchpad, nodeKaraoke, nodeBeer, confirmation) {
        // this._container = container;
        this._pageGenerated = false;
        this._client = new Paho.Client(brokerHostname, brokerPort, clientId);
        this._nodeControl = nodeControl;
        this._nodeLaunchpad = nodeLaunchpad;
        this._nodeKaraoke = nodeKaraoke;
        this._nodeBeer = nodeBeer;
        this._trackedClientNames = [
            this._nodeControl,
            this._nodeLaunchpad,
            this._nodeKaraoke,
            this._nodeBeer
        ];
        this._client.onConnectionLost = this._onConnectionLost.bind(this);
        this._client.onMessageArrived = this._onMessageArrived.bind(this);
        this._brokerStatus = null;
        this._connectivityTable = null;
        this._stopwatch = null;
        this._gpioTable = null;
        this._controlSignalButtons = null;
        this._riddleButtons = null;
        this._launchpadSignalButtons = null;
        this._karaokeSignalButtons = null;
        this._beerSignalButtons = null;
        this._confirmation = confirmation;
        this._intervalId = null;
        this._stopwatchSeconds = 0;
    }

    _setLang(lang) {
        if (confirm(`Are you sure you want to set the game language to "${lang.toUpperCase()}"?`)) {
            this._client.publish(this._nodeControl + '/' + RoomAdmin._topicSetLang, lang);
        }
    }

    generatePage(container) {
        // Header
        let h = document.createElement("h1");
        h.appendChild(document.createTextNode("Room Administration"));
        container.appendChild(h);
        container.appendChild(h);

        // MQTT Broker Status
        let brokerTable = document.createElement("table");
        let brokerRow = brokerTable.insertRow();
        let brokerHeader = brokerRow.insertCell();
        brokerHeader.appendChild(document.createTextNode("Connection to Broker"));
        brokerHeader.style.paddingRight = "10px";
        this._brokerStatus = brokerRow.insertCell();
        this._brokerStatus.style.color = "red";
        this._brokerStatus.appendChild(document.createTextNode("N/A"));
        container.appendChild(brokerTable);

        container.appendChild(document.createElement("br"));

        // MQTT Clients Status
        this._connectivityTable = document.createElement("table");
        this._trackedClientNames.forEach((c) => {
            let clientRow = this._connectivityTable.insertRow();
            let clientHeader = clientRow.insertCell();
            clientHeader.appendChild(document.createTextNode(capitalizeFirstLetter(c)));
            clientHeader.style.paddingRight = "10px";
            let clientStatus = clientRow.insertCell();
            clientStatus.style.color = "red";
            // clientStatus.style.width = "150px";
            // clientStatus.style.fontFamily = "monospace";
            clientStatus.appendChild(document.createTextNode("N/A"));
            /*
            let clientButtonKill = document.createElement("button");
            clientButtonKill.appendChild(document.createTextNode("KILL"));
            clientRow.insertCell().appendChild(clientButtonKill);
            let clientButtonReset = document.createElement("button");
            clientButtonReset.appendChild(document.createTextNode("RESET"));
            clientRow.insertCell().appendChild(clientButtonReset);
             */
        })
        container.appendChild(this._connectivityTable);

        // container.appendChild(document.createElement("br"));

        let controlHeading = document.createElement("h2");
        controlHeading.appendChild(document.createTextNode(capitalizeFirstLetter(this._nodeControl)));
        container.appendChild(controlHeading);

        // Stopwatch
        let stopwatchTable = document.createElement("table");
        let stopwatchRow = stopwatchTable.insertRow();
        let stopwatchHeader = stopwatchRow.insertCell();
        stopwatchHeader.style.paddingRight = "5px";
        stopwatchHeader.appendChild(document.createTextNode("Stopwatch"));
        let stopwatchValue = stopwatchRow.insertCell();
        stopwatchValue.style.fontFamily = "monospace";
        this._stopwatch = document.createTextNode("--:--");
        stopwatchValue.appendChild(this._stopwatch);
        container.appendChild(stopwatchTable);
        container.appendChild(document.createElement("br"));

        // RoomControl GPIO Table
        this._gpioTable = document.createElement("table");
        let arduinoRow = this._gpioTable.insertRow();
        let arduinoHeader = arduinoRow.insertCell();
        arduinoHeader.appendChild(document.createTextNode("Arduino"));
        arduinoHeader.style.paddingRight = "5px";
        for (let i = 0; i < RoomAdmin._numArduinos; i++) {
            let arduinoCell = arduinoRow.insertCell();
            arduinoCell.appendChild(document.createTextNode(i.toString()));
            arduinoCell.colSpan = RoomAdmin._numPins;
            arduinoCell.style.fontFamily = "monospace";
            arduinoCell.style.paddingLeft = "5px";
            arduinoCell.style.paddingRight = "5px";
        }
        let indexRow = this._gpioTable.insertRow();
        let indexHeader = indexRow.insertCell();
        indexHeader.appendChild(document.createTextNode("Index"));
        indexHeader.style.paddingRight = "5px";
        for (let i = 0; i < RoomAdmin._numArduinos; i++) {
            for (let j = 0; j < RoomAdmin._numPins; j++) {
                let indexCell = indexRow.insertCell();
                indexCell.appendChild(document.createTextNode(j.toString()));
                indexCell.style.fontFamily = "monospace";
                indexCell.style.paddingLeft = "5px";
                indexCell.style.paddingRight = "5px";
            }
        }
        ["Input", "Output"].forEach((header,index) => {
            let row = this._gpioTable.insertRow();
            let headerCell = row.insertCell();
            let paddingTop = "0px";
            if (!index) {
                paddingTop = "5px";
            }
            headerCell.appendChild(document.createTextNode(header));
            headerCell.style.paddingRight = "5px";
            headerCell.style.paddingTop = paddingTop;
            for (let i = 0; i < RoomAdmin._numArduinos; i++) {
                for (let i = 0; i < RoomAdmin._numPins; i++) {
                    let cell = row.insertCell();
                    cell.style.fontFamily = "monospace";
                    cell.style.paddingLeft = "5px";
                    cell.style.paddingRight = "5px";
                    cell.style.paddingTop = paddingTop;
                    cell.appendChild(document.createTextNode("/"));
                }
            }
        });
        container.appendChild(this._gpioTable);

        // Language Configuration
        let languageHeading = document.createElement("h3");
        languageHeading.appendChild(document.createTextNode("Set Language"));
        container.appendChild(languageHeading);

        this._languageButtons = document.createElement('p');
        RoomAdmin._langs.forEach((lang) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._setLang.bind(this, lang);
            button.appendChild(document.createTextNode(lang.toUpperCase()));
            this._languageButtons.appendChild(button);
        });
        container.appendChild(this._languageButtons);

        // Signals
        let controlSignalsHeading = document.createElement('h3');
        controlSignalsHeading.appendChild(document.createTextNode("Send Signal"));
        container.appendChild(controlSignalsHeading);

        this._controlSignalButtons = document.createElement('p');
        RoomAdmin._signals[RoomAdmin._control].forEach((signal, index) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._onSignalClick.bind(this, RoomAdmin._control, index);
            button.appendChild(document.createTextNode(signal));
            this._controlSignalButtons.appendChild(button);
        });
        container.appendChild(this._controlSignalButtons);

        let riddleButtonsHeading = document.createElement('h3');
        riddleButtonsHeading.appendChild(document.createTextNode("Solve Riddle"));
        container.appendChild(riddleButtonsHeading);

        this._riddleButtons = document.createElement('p');
        RoomAdmin._riddleNames.forEach((riddleName, index) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._onRiddleClick.bind(this, index);
            button.appendChild(document.createTextNode(riddleName));
            this._riddleButtons.appendChild(button);
        });
        container.appendChild(this._riddleButtons);

        let advancedHeading = document.createElement('h2');
        advancedHeading.appendChild(document.createTextNode("Advanced"));
        container.appendChild(advancedHeading);

        let advancedDesc = document.createElement('p');
        advancedDesc.appendChild(document.createTextNode("These buttons should only be operated, if pressing a control button did not work."));
        container.appendChild(advancedDesc);

        let launchpadHeading = document.createElement('h3');
        launchpadHeading.appendChild(document.createTextNode(capitalizeFirstLetter(this._nodeLaunchpad)));
        container.appendChild(launchpadHeading);
        this._launchpadSignalButtons = document.createElement('p');
        RoomAdmin._signals[RoomAdmin._launchpad].forEach((signal, index) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._onSignalClick.bind(this, RoomAdmin._launchpad, index);
            button.appendChild(document.createTextNode(signal));
            this._launchpadSignalButtons.appendChild(button);
        });
        container.appendChild(this._launchpadSignalButtons);

        let karaokeHeading = document.createElement('h3');
        karaokeHeading.appendChild(document.createTextNode(capitalizeFirstLetter(this._nodeKaraoke)));
        container.appendChild(karaokeHeading);
        this._karaokeSignalButtons = document.createElement('p');
        RoomAdmin._signals[RoomAdmin._karaoke].forEach((signal, index) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._onSignalClick.bind(this, RoomAdmin._karaoke, index);
            button.appendChild(document.createTextNode(signal));
            this._karaokeSignalButtons.appendChild(button);
        });
        container.appendChild(this._karaokeSignalButtons);

        let beerHeading = document.createElement('h3');
        beerHeading.appendChild(document.createTextNode(capitalizeFirstLetter(this._nodeBeer)));
        container.appendChild(beerHeading);
        this._beerSignalButtons = document.createElement('p');
        RoomAdmin._signals[RoomAdmin._beer].forEach((signal, index) => {
            let button = document.createElement('button');
            button.type = 'button';
            button.onclick = this._onSignalClick.bind(this, RoomAdmin._beer, index);
            button.appendChild(document.createTextNode(signal));
            this._beerSignalButtons.appendChild(button);
        });
        container.appendChild(this._beerSignalButtons);

        this._pageGenerated = true;
    }
    _onRiddleClick(riddleIndex) {
        if (!this._confirmation || confirm(`Solve the riddle ${RoomAdmin._riddleNames[riddleIndex]}?`)) {
            this._client.publish(this._nodeControl + '/' + RoomAdmin._topicAdminsolve, RoomAdmin._riddleNames[riddleIndex]);
        }
    }
    _onSignalClick(nodeIndex, signalIndex) {
        if (!this._confirmation || confirm(`Send the signal '${RoomAdmin._signals[nodeIndex][signalIndex]}' to '${this._trackedClientNames[nodeIndex]}'?`)) {
            this._client.publish(this._trackedClientNames[nodeIndex] + '/' + RoomAdmin._topicSignal, RoomAdmin._signals[nodeIndex][signalIndex]);
        }
    }
    _onConnectionLost(responseObject) {
        this._brokerStatus.style.color = "red";
        this._brokerStatus.firstChild.nodeValue = "RECONNECTING";
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:"+responseObject.errorMessage);
        }
        else {
            console.log("onConnectionLost");
        }
    };
    _interpretMessage(msg) {
        let topic = msg.destinationName.split('/');
        if (topic.length >= 2) {
            let nodeIndex = this._trackedClientNames.indexOf(topic[0]);
            if (nodeIndex >= 0) {
                if (topic[1] === "status") {
                    this._connectivityTable.rows[nodeIndex].cells[1].firstChild.nodeValue = msg.payloadString;
                    this._connectivityTable.rows[nodeIndex].cells[1].style.color =
                        msg.payloadString === "ONLINE" ? "green" : "red";
                    return true;
                }
                switch (topic[0]) {
                    case this._nodeControl:
                        let pinType = [RoomAdmin._topicSwitches, RoomAdmin._topicMagnets].indexOf(topic[1]);
                        if (pinType >= 0) {
                            try {
                                let pins = JSON.parse(msg.payloadString);
                                for (let i = 0; i < RoomAdmin._numArduinos; i++) {
                                    if (pins.hasOwnProperty(i)) {
                                        for (let j = 0; j < RoomAdmin._numPins; j++) {
                                            if (pins[i].hasOwnProperty(j)) {
                                                this._gpioTable.rows[pinType + 2]
                                                    .cells[RoomAdmin._numPins * i + j + 1]
                                                    .firstChild.nodeValue = pins[i][j].toString();
                                            }
                                        }
                                    }
                                }
                                return true;
                            } catch (e) {
                                if (e instanceof SyntaxError) {
                                    // console.error(`Cannot parse message ${msg.payloadString} on topic ${msg.destinationName}!`);
                                    return false;
                                } else {
                                    throw e;
                                }
                            }
                        }
                        else if (topic[1] == RoomAdmin._topicRiddles) {
                            try {
                                let riddles = JSON.parse(msg.payloadString);
                                for (let i = 0; i < RoomAdmin._riddleNames.length; i++) {
                                    if (riddles.hasOwnProperty(i)) {
                                        this._riddleButtons.childNodes[i].style.backgroundColor = riddles[i] ? "lightgreen" : "";
                                    }
                                }
                                return true;
                            }
                            catch (e) {
                                if (e instanceof SyntaxError) {
                                    // console.error(`Cannot parse message ${msg.payloadString} on topic ${msg.destinationName}!`);
                                    return false;
                                } else {
                                    throw e;
                                }
                            }
                        }
                        else if (topic[1] == RoomAdmin._topicTime) {
                            if (msg.payloadString.length == 0 || msg.payloadString === "-") {
                                this._stopwatch.textContent = "--:--";
                                return true;
                            }
                            else {
                                let time = msg.payloadString.split(';');
                                if (time.length <= 0) {
                                    console.error("Time string is invalid");
                                }
                                else {
                                    let start = new Date(time[0]);
                                    if (time.length > 1) {
                                        let end = new Date(time[1]);
                                        if (this._intervalId !== null) {
                                            clearInterval(this._intervalId);
                                        }
                                        let diff = end.getTime() - start.getTime();
                                        diff = Math.floor(diff / 1000); // get seconds
                                        this._stopwatchSeconds = diff;
                                        this._stopwatch.textContent = stopwatchString(diff);
                                    }
                                    else {
                                        let now = new Date();
                                        let diff = now.getTime() - start.getTime();
                                        diff = Math.floor(diff / 1000); // get seconds
                                        this._stopwatchSeconds = diff;
                                        this._stopwatch.textContent = stopwatchString(diff);
                                        if (this._intervalId !== null) {
                                            clearInterval(this._intervalId);
                                        }
                                        this._intervalId = setInterval(() => {
                                            let sec = ++(this._stopwatchSeconds);
                                            this._stopwatch.textContent = stopwatchString(sec);
                                        }, 1000);
                                    }
                                    return true;
                                }
                            }
                        }
                        else if (topic.slice(1).join('/') == RoomAdmin._topicGetLang) {
                            let lang = msg.payloadString.toLowerCase();
                            let index = RoomAdmin._langs.indexOf(lang);
                            // If it is none of the supported languages, all buttons will become not highlighted.
                            this._languageButtons.childNodes.forEach((element, i) => {
                                let color = "";
                                if (i == index) {
                                    color = "lightgreen";
                                }
                                element.style.backgroundColor = color;
                            });
                            return true;
                        }
                        break;
                    case this._nodeLaunchpad:
                        break;
                }
            }
        }
        return false;
    }
    _onMessageArrived(msg) {
        if (!this._interpretMessage(msg)) {
            console.error("Unknown message " + msg.payloadString + " on topic " + msg.destinationName + ".");
        }
    };
    _onConnect() {
        this._brokerStatus.style.color = "green";
        this._brokerStatus.firstChild.nodeValue = "CONNECTED";
        this._trackedClientNames.forEach((client) =>
            this._client.subscribe(client + '/' + RoomAdmin._topicStatus)
        );
        this._client.subscribe(this._nodeControl + '/' + RoomAdmin._topicSwitches);
        this._client.subscribe(this._nodeControl + '/' + RoomAdmin._topicMagnets);
        this._client.subscribe(this._nodeControl + '/' + RoomAdmin._topicRiddles);
        this._client.subscribe(this._nodeControl + '/' + RoomAdmin._topicTime);
        this._client.subscribe(this._nodeControl + '/' + RoomAdmin._topicGetLang);
    }
    _onConnectFail(obj) {
        console.error("Connect failed: " + obj.errorMessage);
        this._connect();
    }
    _connect() {
        this._client.connect({
            "reconnect": true,
            "onSuccess": this._onConnect.bind(this),
            "cleanSession": false,
            "keepAliveInterval": 30,
            "onFailure": this._onConnectFail.bind(this)
        });
    }
    run() {
        if (!this._pageGenerated) {
            console.error("Page not generated yet!");
            return;
        }
        this._brokerStatus.firstChild.nodeValue = "CONNECTING";
        this._connect();
    }
}

RoomAdmin._control = 0;
RoomAdmin._launchpad = 1;
RoomAdmin._karaoke = 2;
RoomAdmin._beer = 3;
RoomAdmin._signals = [
    [
        "RESETLIGHT",
        "RESETLIGHTOFF",
        "RESET"
    ],
    [
        "ACTIVATE",
        "RESET",
        "ADMINSOLVE"
    ],
    [
        "ACTIVATE",
        "RESET",
        "ADMINSOLVE"
    ],
    [
        "ACTIVATE",
        "RESET",
        "ADMINSOLVE"
    ],
];

RoomAdmin._langs = ["de", "en"];
RoomAdmin._numArduinos = 2;
RoomAdmin._numPins = 8;
RoomAdmin._topicStatus = "status";
RoomAdmin._topicSignal = "signal";
RoomAdmin._topicTime = "time";
RoomAdmin._topicSwitches = "switches";
RoomAdmin._topicMagnets = "magnets";
RoomAdmin._topicRiddles = "riddles";
RoomAdmin._topicAdminsolve = "adminsolve";
RoomAdmin._topicSetLang = "set/lang";
RoomAdmin._topicGetLang = "get/lang";
RoomAdmin._riddleNames = [
    "PREGAME",
    "STARTGAME",
    "FUSES",
    "STAFF",
    "SWITCH",
    "TUBES",
    "RESTROOM",
    "ALARM",
    "GUEST",
    "TABLE",
    "KARAOKE",
    "PICS",
    "LAUNCHPAD",
    "STICKER",
    "DICES",
    "SAFE",
    "BEER",
    "DOOR"
];

let roomAdmin = null;

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}

/**
 * Deep merge two objects.
 * @param target
 * @param ...sources
 */
function mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return mergeDeep(target, ...sources);
}

window.onload = function() {
    let finalConfig = defaultConfig;
    if (typeof config !== 'undefined') {
        mergeDeep(finalConfig, config);
    }
    console.log(finalConfig);
    roomAdmin = new RoomAdmin(finalConfig.broker.host, finalConfig.broker.port, finalConfig.clientId + Date.now(),
        finalConfig.nodes.control, finalConfig.nodes.launchpad, finalConfig.nodes.karaoke, finalConfig.nodes.beer, finalConfig.confirmation);
    roomAdmin.generatePage(document.body);
    roomAdmin.run();
}
